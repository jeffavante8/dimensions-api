<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mechanic extends Model
{
    public function type()
    {
        return $this->hasOne('App\Mechanic\Type');
    }

    public function nodes()
    {
        return $this->belongsToMany('App\Mechanic');
    }
}

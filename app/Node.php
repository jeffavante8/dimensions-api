<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    public function type()
    {
        return $this->hasOne('App\Node\Type');
    }

    public function decisions()
    {
        return $this->hasMany('App\Node\Decision');
    }

    public function mechanics()
    {
        return $this->morphToMany('App\Mechanic', 'triggerable');
    }
}

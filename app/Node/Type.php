<?php

namespace App\Node;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'node_types';
}

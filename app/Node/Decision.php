<?php

namespace App\Node;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    protected $table = 'node_decisions';

    public function destination_node()
    {
        return $this->belongsTo('App\Node', 'destination_node_id', 'id');
    }

    public function node()
    {
        return $this->belongsTo('App\Node');
    }

    public function mechanics()
    {
        return $this->morphToMany('App\Mechanic', 'triggerable');
    }
}

<?php

namespace App\Http\Controllers;

use App\Game;
use App\Node;
use App\Node\Decision;

use Illuminate\Http\Request;
use App\Http\Resources\Game as GameResource;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO: Set node_id to root node of story
        $game =  Game::create([
            'story_id' => $request->get('story_id'),
            'user_id' => $request->get('user_id')
        ]);

        $firstNode = Node::where('story_id', $request->get('story_id'))
            ->whereNull('parent_id')
            ->first();

        if ($firstNode) {
            $game->node_id = $firstNode->id;
            $game->save();
        }

        return new GameResource($game);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function decision(Game $game, Decision $decision)
    {
        $game->node_id = $decision->destination_node_id;
        $game->save();

        $game->load('story', 'node');

        foreach ($decision->mechanics as $mechanic)
            $game->triggerMechanic($mechanic);


        $node = \App\Node::find($game->node_id)->load('decisions');

        //TODO: Trigger mechanics when exiting node? 
        foreach ($node->mechanics as $mechanic)
            $game->triggerMechanic($mechanic);

        //TODO: Add to resource model insteadl of duplicating logic
        $resource = new GameResource($game);
        $resource->additional(['data' => ['decisions' => $node->decisions]]);
        return $resource;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        $game->load('story', 'node');

        $resource = new GameResource($game);
        $node = \App\Node::find($game->node_id)->load('decisions', 'mechanics');

        $resource->additional(['data' => ['decisions' => $node->decisions, 'mechanics' => $node->mechanics]]);
        return $resource;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    public function nodes()
    {
        return $this->hasMany('App\Node');
    }

    public function games()
    {
        return $this->hasMany('App\Games');
    }
}

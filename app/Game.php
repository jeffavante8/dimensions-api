<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GameVariable;

class Game extends Model
{
    protected $fillable = [
        "story_id",
        "user_id",
    ];

    public function story()
    {
        return $this->belongsTo(\App\Story::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function node()
    {
        return $this->belongsTo(\App\Node::class);
    }

    public function variables()
    {
        return $this->hasMany(\App\GameVariable::class);
    }

    //TODO: Have variables cascade in to game, then save when game is saved?
    public function setVariable($key, $data) {
        $variable = GameVariable::where('variable',$key)->where('game_id', $this->id)->first();
        if (!$variable) {
            $variable = GameVariable::create(
                ['game_id' => $this->id, 
                'variable' => $key, 
                'data' => $data]);
            return $variable;
        }
        $variable->data = $data;
        $variable->save();
        return $variable;
    }

    public function getVariable($key) {
        $variable = GameVariable::where('variable',$key)->where('game_id', $this->id)->first();
        if (!$variable) {
            return false;
        }
        return $variable->data;
    }

    /**
     * Prepend story text to node, to be called from a mechanic
     *
     * @param [type] $text
     * @return void
     */
    public function prependStory($text) {
        $this->node->description = "<p>" . $text . "</p>" .  $this->node->description;
    }

    /**
     * Append story text to node, to be called from a mechanic
     *
     * @param [type] $text
     * @return void
     */
    public function appendStory($text) {
        $this->node->description .= "<p>" . $text . "</p>";
    }

    public function triggerMechanic($mechanic) {
        //! IMPORTANT
        //TODO: Sandbox this instead of just eval
        eval($mechanic->action);
    }
}

 
 
 
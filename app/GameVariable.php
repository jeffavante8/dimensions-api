<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameVariable extends Model
{
    protected $fillable = [
        'game_id', 'variable', 'data',
    ];
    protected $table = 'game_variables';
    public $timestamps = false;
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMechanicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mechanics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('action');
            $table->timestamps();
        });

        Schema::create('triggerables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mechanic_id');
            $table->integer('triggerable_id');
            $table->text('triggerable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mechanics');
        Schema::dropIfExists('triggerables');
    }
}
